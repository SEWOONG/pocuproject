#include <cassert>
#include <iostream>

#include "Node.h"
#include "DoublyLinkedList.h"

using namespace lab10;

int main()
{
	std::unique_ptr<int> number1 = std::make_unique<int>(1);
	std::shared_ptr<Node<int>> node1 = std::make_shared<Node<int>>(std::move(number1));
	assert(*node1->Data == 1);
	assert(node1->Next == nullptr);
	assert(node1->Previous.expired() == true);
	std::cout << "Test Node(std::unique_ptr<T> data) constructor: PASS" << std::endl;

	std::unique_ptr<int> number2 = std::make_unique<int>(2);
	std::shared_ptr<Node<int>> node2 = std::make_shared<Node<int>>(std::move(number2), node1);
	node1->Next = node2;
	assert((*node2->Data) == 2);
	assert((*node1->Next->Data) == 2);
	std::shared_ptr<Node<int>> prevNode2 = node2->Previous.lock();
	assert((*prevNode2->Data) == 1);
	std::cout << "Test Node(std::unique_ptr<T> data, std::weak_ptr<T> prev) constructor: PASS" << std::endl;

	assert(sizeof(DoublyLinkedList<int>) <= 16);
	std::cout << "Test sizeof(DoublyLinkedList<T>) - less equal than 16 bytes: PASS: " << std::endl;

	DoublyLinkedList<int> list;
	std::shared_ptr<Node<int>> head = list[0];
	assert(head == nullptr);
	std::cout << "Test DoublyLinkedList<T>() constructor: PASS" << std::endl;

	list.Insert(std::make_unique<int>(1), 0);
	assert(list.GetLength() == 1);
	std::shared_ptr<Node<int>> testNode = list[0];
	assert(*testNode->Data == 1);
	assert(testNode->Next == nullptr);
	assert(testNode->Previous.lock() == nullptr);
	std::cout << "Test Insert(data, index) - initial List: PASS" << std::endl;

	list.Insert(std::make_unique<int>(2));
	assert(*testNode->Next->Data == 2);
	assert(list.GetLength() == 2);
	testNode = list[1];
	assert(*testNode->Data == 2);
	std::cout << "Test Insert(data): PASS" << std::endl;

	list.Insert(std::make_unique<int>(3));
	list.Insert(std::make_unique<int>(4));
	list.Insert(std::make_unique<int>(6));
	list.Insert(std::make_unique<int>(7));
	assert(*list[0]->Data == 1);
	assert(*list[1]->Data == 2);
	assert(*list[2]->Data == 3);
	assert(*list[3]->Data == 4);
	assert(*list[4]->Data == 6);
	assert(*list[5]->Data == 7);
	std::cout << "Test Insert(data) - continuous Inserting: PASS" << std::endl;

	list.Insert(std::make_unique<int>(5), 4);
	assert(*list[0]->Data == 1);
	assert(*list[1]->Data == 2);
	assert(*list[2]->Data == 3);
	assert(*list[3]->Data == 4);
	assert(*list[4]->Data == 5);
	assert(*list[5]->Data == 6);
	assert(*list[6]->Data == 7);
	std::cout << "Test Insert(data, index) - Insert data in the middle: PASS" << std::endl;

	for (size_t index = 0; index < list.GetLength() - 1; index++)
	{
		auto data1 = *list[index]->Data;
		auto data2 = *list[index + 1]->Previous.lock()->Data;
		assert(data1 == data2);
	}
	std::cout << "Test Insert() - Check the bidirectional links: PASS" << std::endl;

	unsigned int size = list.GetLength();
	assert(size == 7);
	std::cout << "Test GetLength(): PASS" << std::endl;

	assert(list.Search(4));
	std::cout << "Test Search(data): PASS" << std::endl;
	assert(list.Search(10) == false);
	std::cout << "Test Search(data) - not found: PASS" << std::endl;

	assert(list.Delete(3));
	assert(list.GetLength() == 6);
	std::cout << "Test Delete(data): PASS" << std::endl;

	assert(list.Delete(3) == false);
	assert(list.GetLength() == 6);
	std::cout << "Test Delete(data) - not found: PASS" << std::endl;

	assert(list.Delete(1));
	assert(list.GetLength() == 5);
	std::cout << "Test Delete(data) - first node: PASS" << std::endl;

	assert(list.Delete(7));
	assert(list.GetLength() == 4);
	std::cout << "Test Delete(data) - last node: PASS" << std::endl;

	list.Insert(std::make_unique<int>(1), 0);
	assert(list.GetLength() == 5);
	std::shared_ptr<Node<int>> firstNode = list[0];
	assert(*firstNode->Data == 1);
	std::cout << "Test Insert(data, index) - first index: PASS" << std::endl;

	std::shared_ptr<Node<int>> node = list[2];
	assert(*node->Data == 4);
	list.Insert(std::make_unique<int>(10), 2);
	node = list[2];
	assert(*node->Data == 10);
	assert(list.GetLength() == 6);
	std::cout << "Test Insert(data, index): PASS" << std::endl;

	list.Insert(std::make_unique<int>(11), 10);
	assert(list.GetLength() == 7);
	std::shared_ptr<Node<int>> lastNode = list[6];
	assert(*lastNode->Data == 11);
	std::cout << "Test Insert(data, index) - index out of range: PASS" << std::endl;

	list.Delete(1);
	list.Delete(2);
	list.Delete(10);
	list.Delete(4);
	list.Delete(5);
	list.Delete(6);
	list.Delete(11);

	list.Insert(std::make_unique<int>(1));
	node = list[0];
	assert(*node->Data == 1);
	std::cout << "Test Insert(data) - Insert() again after cleared list: PASS" << std::endl;

	list.Delete(1);
	list.Insert(std::make_unique<int>(2), 1);
	node = list[0];
	assert(*node->Data == 2);
	std::cout << "Test Insert(data, index) - Insert() again after cleared list: PASS" << std::endl;

	return 0;
}