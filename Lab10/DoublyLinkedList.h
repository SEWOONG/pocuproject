#pragma once

#include <memory>

namespace lab10
{
	template<typename T>
	class Node;

	template<typename T>
	class DoublyLinkedList
	{
	public:
		DoublyLinkedList();
		void LFirst();
		void LNext();
		void LEnd();
		void Insert(std::unique_ptr<T> data);
		void Insert(std::unique_ptr<T> data, unsigned int index);
		bool Delete(const T& data);
		bool Search(const T& data) const;

		std::shared_ptr<Node<T>> operator[](unsigned int index) const;
		unsigned int GetLength() const;

	private:
		unsigned int mLength;
		std::shared_ptr<Node<T>> mHead;
		std::shared_ptr<Node<T>> mCur;
		std::shared_ptr<Node<T>> mTail;
	};

	template<typename T> DoublyLinkedList<T>::DoublyLinkedList()
		: mLength(0)
	{
		mHead = std::make_shared<Node<T>>();
		mCur = std::make_shared<Node<T>>();
		mTail = std::make_shared<Node<T>>();

		mHead = nullptr;
		mTail = nullptr;

		mHead->Next = mCur;
		mCur->Previous = mHead;

		mCur->Next = mTail;
		mTail->Previous = mCur;
	}

	template<typename T> void DoublyLinkedList<T>::LFirst()
	{
		mCur = mHead->Next;
	}

	template<typename T> void DoublyLinkedList<T>::LNext()
	{
		mCur = mCur->Next;
	}

	template<typename T> void DoublyLinkedList<T>::LEnd()
	{
		LFirst();
		while (mCur->Next != mTail)
		{
			LNext();
		}
	}

	template<typename T> void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data)
	{
		if (GetLength() == 0)
		{
			LFirst();

			mCur->Data = std::move(data);
			mLength++;
		}
		else
		{
			LEnd();
			
			std::shared_ptr<Node<T>> newNode = std::make_shared<Node<T>>(std::move(data), mCur);
			mCur->Next = newNode;
			newNode->Next = mTail;
			mTail->Previous = newNode;

			mLength++;
		}
	}

	template<typename T> void DoublyLinkedList<T>::Insert(std::unique_ptr<T> data, unsigned int index)
	{
		//index가 리스트의 총 길이보다 크거나 같을 경우
		if (index >= GetLength())
		{
			LEnd();

			std::shared_ptr<Node<T>> newNode = std::make_shared<Node<T>>(std::move(data), mCur);
			mCur->Next = newNode;
			newNode->Next = mTail;
			mTail->Previous = newNode;

			mLength++;
			return;
		}
		
		if (index == 0 && GetLength() == 0)
		{
			LFirst();

			mCur->Data = std::move(data);
			mLength++;

			return;
		}
		else if(index == 0 && GetLength() != 0)
		{
			LFirst();

			std::shared_ptr<Node<T>> newNode = std::make_shared<Node<T>>(std::move(data), mHead);
			mHead->Next = newNode;
			newNode->Next = mCur;
			mCur->Previous = newNode;
			mLength++;

			return;
		}

		std::shared_ptr<Node<T>> newNode = std::make_shared<Node<T>>(std::move(data));

		LFirst();
		for (unsigned int i = 0; i < index; i++)
		{
			LNext();
		}
		newNode->Next = mCur;
		mCur->Previous = newNode;

		LFirst();
		for (unsigned int i = 0; i < index - 1; i++)
		{
			LNext();
		}
		mCur->Next = newNode;
		newNode->Previous = mCur;

		mLength++;

	}

	template<typename T> bool DoublyLinkedList<T>::Delete(const T& data)
	{
		LFirst();
		std::shared_ptr<Node<T>> previousNode = std::make_shared<Node<T>>();
		while (1)
		{
			if (mCur == mTail)
			{
				return false;
			}

			if (*mCur->Data == data)
			{
				break;
			}
			LNext();
		}

		previousNode = mCur->Previous.lock();

		previousNode->Next = mCur->Next;
		mCur->Next->Previous = previousNode;

		mCur.reset();
		mLength--;
		return true;
	}

	template<typename T> bool DoublyLinkedList<T>::Search(const T& data) const
	{
		std::shared_ptr<Node<T>> searchData = std::make_shared<Node<T>>();
		searchData = mHead->Next;

		while (1)
		{
			if (searchData == mTail)
			{
				searchData.reset();
				return false;
			}

			if (*searchData->Data == data)
			{
				searchData.reset();
				return true;
			}
			else
			{
				searchData = searchData->Next;
			}
		}
		searchData.reset();
		return false;
	}

	template<typename T> std::shared_ptr<Node<T>> DoublyLinkedList<T>::operator[](unsigned int index) const
	{
		auto x = std::make_unique<T>();
		std::shared_ptr<Node<T>> temp = std::make_shared<Node<T>>(std::move(x));
		temp = mHead->Next;
		for (unsigned int i = 0; i < index; i++)
		{
			temp = temp->Next;
		}

		return std::move(temp);
	}

	template<typename T> unsigned int DoublyLinkedList<T>::GetLength() const
	{
		return mLength;
	}
}