#pragma once

namespace lab8
{
	template <size_t N>
	class FixedVector<bool, N>
	{
	public:
		FixedVector()
			: mSize(0)
		{
		}

		~FixedVector()
		{
		}

		bool Add(const bool& t)
		{
			if (mSize >= N)
			{
				return false;
			}

			if (t == true)
			{
				mData += '1';
			}
			else
			{
				mData += '0';
			}

			mSize++;
			return true;
		}

		bool Remove(const bool& t)
		{
			bool b;
			for (unsigned int i = 0; i < mSize; i++)
			{
				if (mData[i] == '1')
				{
					b = true;
				}
				else
				{
					b = false;
				}

				if (b == t)
				{
					mData[i] = NULL;

					for (unsigned int j = i; j < mSize - 1; j++)
					{
						mData[j] = mData[j + 1];
					}
					mData[mSize - 1] = NULL;
					mSize--;

					return true;
				}
			}
			return false;
		}

		bool Get(unsigned int index) const
		{
			if (mData[index] == '1')
			{
				return true;
			}

			return false;
		}

		//잘모르겠음.
		bool operator[](size_t index) const
		{
			if (mData[index] == '1')
			{
				return true;
			}
			return false;
		}

		int GetIndex(bool t) const
		{
			for (unsigned int i = 0; i < mSize; i++)
			{
				if (mData[i] == '1' && t == true)
				{
					return static_cast<int>(i);
				}
				else if (mData[i] == '0' && t == false)
				{
					return static_cast<int>(i);
				}
			}
			return -1;
		}

		size_t GetSize() const
		{
			return mSize;
		}

		size_t GetCapacity() const
		{
			return N;
		}

	private:
		std::string mData;
		size_t mSize;

	};
}