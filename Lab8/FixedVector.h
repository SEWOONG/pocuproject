#pragma once

namespace lab8
{
	template <typename T, size_t N>
	class FixedVector
	{
	public:
		FixedVector()
			: mSize(0)
		{
			mData = new T[N];
		}
		
		~FixedVector()
		{
			delete[] mData;
		}

		bool Add(const T& t)
		{
			if (mSize >= N)
			{
				return false;
			}

			mData[mSize++] = t;
			return true;
		}

		bool Remove(const T& t)
		{
			for (unsigned int i = 0; i < mSize; i++)
			{
				if (mData[i] == t)
				{
					mData[i] = NULL;
					 
					for (unsigned int j = i; j < mSize - 1; j++)
					{
						mData[j] = mData[j + 1];
					}
					mData[mSize - 1] = NULL;
					mSize--;

					return true;
				}
			}
			return false;
		}

		T& Get(unsigned int index) const
		{
			return mData[index];
		}

		T& operator[](size_t index)
		{
			return mData[index];
		} 

		int GetIndex(T t) const
		{
			for (unsigned int i = 0; i < mSize; i++)
			{
				if (mData[i] == t)
				{
					return static_cast<int>(i);
				}
			}
			return -1;
		}

		size_t GetSize() const
		{
			return mSize;
		}

		size_t GetCapacity() const
		{
			return N;
		}

	private:
		T* mData;
		size_t mSize;

	};
}