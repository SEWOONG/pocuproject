#pragma once

#include <iostream>
#include <iterator>
#include <algorithm>
#include <vector>
#include <map>

namespace lab7
{
	template <typename K, class V>
	std::map<K, V> ConvertVectorsToMap(const std::vector<K>& keys, const std::vector<V>& values)
	{
		std::map<K, V> m;
		unsigned int length;
		keys.size() >= values.size() ? length = values.size() : length = keys.size();
		unsigned int keyIndex = 0;
		unsigned int valuesIndex = 0;

		for (unsigned int i = 0; i < length; i++)
		{
			if (i > 0)
			{
				for (unsigned int j = 0; j < i - 1; j++)
				{
					if (keys.at(j) == keys.at(i))
					{
						keyIndex++;
					}
				}
			}
			if (keyIndex >= keys.size() || valuesIndex >= values.size())
			{
				break;
			}

			m.insert(std::pair<K, V>(keys.at(keyIndex), values.at(valuesIndex)));
			keyIndex++;
			valuesIndex++;
		}
		return m;
	}

	template <typename K, class V>
	std::vector<K> GetKeys(const std::map<K, V>& m)
	{
		std::vector<K> v;
		for (auto iter = m.begin(); iter != m.end(); iter++)
		{
			v.push_back(iter->first);
		}

		return v;
	}

	template <typename K, class V>
	std::vector<V> GetValues(const std::map<K, V>& m)
	{
		std::vector<V> v;
		for (auto iter = m.begin(); iter != m.end(); iter++)
		{
			v.push_back(iter->second);
		}

		return v;
	}

	template <typename T>
	std::vector<T> Reverse(const std::vector<T>& v)
	{
		std::vector<T> rv;
		for (auto it = v.rbegin(); it != v.rend(); it++)
		{
			rv.push_back(*it);
		}

		return rv;
	}
}

template <typename T>
std::vector<T> operator+(const std::vector<T>& v1, const std::vector<T>& v2)
{
	std::vector<T> combined;
	unsigned int eCount = 0;
	std::vector<unsigned int> eIndex;
	eIndex.reserve(20);

	for (auto it = v1.begin(); it != v1.end(); it++)
	{
		combined.push_back(*it);
	}
	for (auto it = v2.begin(); it != v2.end(); it++)
	{
		combined.push_back(*it);
	}
	
	for (auto iter = combined.begin(); iter != combined.end(); iter++)
	{
		for (auto iter2 = combined.begin(); iter2 != iter; iter2++)
		{
			if (*iter == *iter2)
			{
				eIndex.push_back(eCount);
			}
		}

		eCount++;
	}

	if (eIndex.size() == 0)
	{
		return combined;
	}

	for (unsigned int i = eIndex.size() - 1; ; i--)
	{
		auto it = combined.begin();
		for (unsigned int j = 0; j < eIndex[i]; j++)
		{
			it++;
		}
		combined.erase(it);

		if (i == 0)
		{
			break;
		}
	}
	return combined;
}

template <typename K, class V>
std::map<K, V> operator+(const std::map<K, V>& m1, const std::map<K, V>& m2)
{
	std::map<K, V> combined;
	auto it = m1.begin();
	auto it2 = m2.begin();
	unsigned int overlab = 0;
	for (it; it != m1.end(); it++)
	{
		combined.insert(std::pair<K, V>(it->first, it->second));
	}
	for (it2; it2 != m2.end(); it2++)
	{
		for (it = m1.begin(); it != m1.end(); it++)
		{
			if (it->first == it2->first)
			{
				overlab = 1;
				break;
			}
		}

		if (overlab == 1)
		{
			overlab = 0;
			continue;
		}

		combined.insert(std::pair<K, V>(it2->first, it2->second));
	}

	return combined;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
	for (auto it = v.begin(); it != v.end(); it++)
	{
		if (it != v.begin())
		{
			os << ", ";
		}
		os << *it;
	}
	return os;
}

template <typename K, class V>
std::ostream& operator<<(std::ostream& os, const std::map<K, V>& m)
{
	for (auto it = m.begin(); it != m.end(); it++)
	{
		os << "{ " << it->first << ", " << it->second << " }" << std::endl;
	}
	return os;
}