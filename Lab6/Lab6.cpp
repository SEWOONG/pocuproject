#include "Lab6.h"
namespace lab6
{
	int Sum(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}
		int sum = 0;

		for (unsigned int i = 0; i < v.size(); i++)
		{
			sum += v[i];
		}

		return sum;
	}

	int Min(const std::vector<int>& v)
	{
		int min = INT32_MAX;
		
		if (v.size() == 0)
		{
			return min;
		}

		for (unsigned int i = 0; i < v.size(); i++)
		{
			if (v[i] < min)
			{
				min = v[i];
			}
		}

		return min;
	}

	int Max(const std::vector<int>& v)
	{
		int max = INT32_MIN;

		for (unsigned int i = 0; i < v.size(); i++)
		{
			if (v[i] > max)
			{
				max = v[i];
			}
		}

		return max;
	}

	float Average(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}

		float sum = 0.f;
		float average = 0.f;

		for (unsigned int i = 0; i < v.size(); i++) //end에 () 없음
		{
			sum += v[i];
		}

		average = sum / static_cast<float>(v.size());

		return average;
	}

	int NumberWithMaxOccurrence(const std::vector<int>& v)
	{
		if (v.size() == 0)
		{
			return 0;
		}

		std::vector<int> vCount;
		vCount.reserve(v.size());
		vCount.assign(v.size(), 0);

		unsigned int i, j;
		for (i = 0; i < v.size(); i++) //end에 () 없음
		{
			for (j = 0; j < i; j++)
			{
				if (v[i] == v[j])
				{
					vCount[i] = vCount[i] + 1;
				}
			}
		}

		int maxOccurrence = v[Max(vCount) + 1];

		if (maxOccurrence == 0)
		{
			return v[0];
		}
		return maxOccurrence;
	}

	void SortDescending(std::vector<int>& v)
	{
		for (unsigned int i = 0; i < v.size(); i++)
		{
			for (unsigned int j = 0; j < v.size() - 1; j++)
			{
				if (v[j] < v[j + 1])
				{					int swap = v[j];
					v[j] = v[j + 1];
					v[j + 1] = swap;
				}
			}
		}
	}

}