#include <cassert>

#include "Storage.h"

using namespace std;
using namespace lab11;

void Do()
{
	const unsigned int SIZE = 10000;

	Storage<int> storage1(SIZE, 0);
	const std::unique_ptr<int[]>& data1 = storage1.GetData();
	for (int i = 0; i < SIZE; ++i)
	{
		assert(data1[i] == 0);
	}


	Storage<int> storage2(SIZE, 100);
	const std::unique_ptr<int[]>& data2 = storage2.GetData();
	
	for (int i = 0; i < SIZE; ++i)
	{
		assert(data2[i] == 100);
	}

	// using move constructor
	Storage<int> storage3(SIZE, 50);
	const std::unique_ptr<int[]>& data3 = storage3.GetData();

	assert(storage3.GetSize() == SIZE);

	for (int i = 0; i < SIZE; ++i)
	{
		assert(data3[i] == 50);
	}

	/*
	Storage(const Storage& other);
	Storage(Storage&& other);
	Storage<T> operator=(const Storage& other) const;
	Storage<T>& operator=(Storage&& other);
	*/

	Storage<int> storage3Copy(std::move(storage3));
	const std::unique_ptr<int[]>& data3Copy = storage3Copy.GetData();

	assert(storage3Copy.GetSize() == SIZE);
	for (int i = 0; i < SIZE; ++i)
	{
		assert(data3Copy[i] == 50);
	}
	const std::unique_ptr<int[]>& my = storage3.GetData();
	assert(storage3.GetData() == nullptr);
	assert(storage3.GetSize() == 0);

	Storage<char> storage4(SIZE, 'f');
	const std::unique_ptr<char[]>& data4 = storage4.GetData();

	assert(storage4.GetSize() == SIZE);

	for (int i = 0; i < SIZE; ++i)
	{
		assert(data4[i] == 'f');
	}

	Storage<char> storage4Copy = std::move(storage4);
	const std::unique_ptr<char[]>& data4Copy = storage4Copy.GetData();

	assert(storage4Copy.GetSize() == SIZE);

	for (int i = 0; i < SIZE; ++i)
	{
		assert(data4Copy[i] == 'f');
	}

	assert(storage4.GetData() == nullptr);
	assert(storage4.GetSize() == 0);

	const unsigned int SIZE2 = 5000;

	Storage<int> storage5(SIZE2);
	const std::unique_ptr<int[]>& data5 = storage5.GetData();

	for (int i = 0; i < SIZE2; ++i)
	{
		storage5.Update(i, SIZE2 - i - 1);
		assert(data5[i] == SIZE2 - i - 1);
	}
}

int main()
{
	Do();
	
	//복사생성자와 이동생성자
	Storage<int> s1(10000, 10);
	Storage<int> s2(s1);

	const unique_ptr<int[]>& temp1 = s1.GetData();
	const unique_ptr<int[]>& temp2 = s2.GetData();
	
	for (unsigned int i = 0; i < 10000; i++)
	{
		assert(temp1[i] == 10);
		assert(temp2[i] == 10);
	}

	Storage<int> s3(move(s1));
	const unique_ptr<int[]>& temp3 = s3.GetData();
	for (unsigned int i = 0; i < 10000; i++)
	{
		assert(temp3[i] == 10);
	}
	
	assert(s1.GetSize() == 0);

	//살아있는 개체 s2, s3
	//이동대입 연산자
	
	Storage<int> sCopy(10000);
	sCopy = move(s2);
	const unique_ptr<int[]>& tempCopy1 = sCopy.GetData();
	for (unsigned int i = 0; i < 10000; i++)
	{
		assert(tempCopy1[i] == 10);
	}
	assert(s2.GetSize() == 0);

	//살아있는 개체 s3, sCopy
	//대입연산자

	Storage<int> sCopy2(10000);
	sCopy2 = s3;
	const unique_ptr<int[]>& tempCopy2 = sCopy2.GetData();
	for (unsigned int i = 0; i < 10000; i++)
	{
		assert(tempCopy2[i] == 10);
	};
	for (unsigned int i = 0; i < 10000; i++)
	{
		assert(temp3[i] == 10);
	};
	
	//임시 끝
	return 0;
}