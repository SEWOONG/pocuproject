#pragma once
#include <memory>

using namespace std;

namespace lab11
{
	template<typename T>
	class Storage
	{
	public:
		Storage(unsigned int length);
		Storage(unsigned int length, const T& initialValue);
		~Storage() = default;
		Storage(const Storage& other);
		Storage(Storage&& other);
		Storage<T> operator=(const Storage& other);
		Storage<T>& operator=(Storage&& other);

		bool Update(unsigned int index, const T& data);
		const std::unique_ptr<T[]>& GetData() const;
		unsigned int GetSize() const;

	private:
		unique_ptr<T[]> mStorage;
		unsigned int mLength;
	};

	template<typename T> Storage<T>::Storage(unsigned int length)
		: mLength(length)
	{
		mStorage = make_unique<T[]>(mLength);
		std::fill_n(mStorage.get(), mLength, 0);
	}

	template<typename T> Storage<T>::Storage(unsigned int length, const T& initialValue)
		: mLength(length)
	{
		mStorage = make_unique<T[]>(mLength);
		std::fill_n(mStorage.get(), mLength, initialValue);
	}

	template<typename T> Storage<T>::Storage(const Storage& other)
		: mLength(other.mLength)
	{
		mStorage = make_unique<T[]>(mLength);
		memcpy(mStorage.get(), other.mStorage.get(), mLength * sizeof(T));
	}

	template<typename T> Storage<T>::Storage(Storage&& other)
		: mLength(0)
		, mStorage(nullptr)
	{	
		*this = move(other);
	}

	template<typename T> Storage<T> Storage<T>::operator=(const Storage& other)
	{
		mLength = other.mLength;
		memcpy(mStorage.get(), other.mStorage.get(), sizeof(T) * mLength);

		return *this;
	}

	template<typename T> Storage<T>& Storage<T>::operator=(Storage&& other)
	{
		if (this != &other)
		{
			mStorage = nullptr;
			mLength = 0;

			mStorage = move(other.mStorage);
			mLength = other.mLength;

			other.mStorage = nullptr;
			other.mLength = 0;
		}
		return *this;
	}

	template<typename T> bool Storage<T>::Update(unsigned int index, const T& data)
	{
		if (index >= mLength)
		{
			return false;
		}

		mStorage[index] = data;
		return true;
	}

	template<typename T> const std::unique_ptr<T[]>& Storage<T>::GetData() const
	{
		const std::unique_ptr<T[]>& temp = mStorage;
		return std::move(temp);
	}

	template<typename T> unsigned int Storage<T>::GetSize() const
	{
		return mLength;
	}
}