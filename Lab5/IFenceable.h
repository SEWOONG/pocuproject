#pragma once
#include "eFenceType.h"

namespace lab5
{
	class IFenceable
	{
	public:
		virtual unsigned int GetMinimumFencesCount() const = 0; //최소 담장개수
		virtual unsigned int GetFencePrice(eFenceType fenceType) const = 0; //담장의 가격
	};
}