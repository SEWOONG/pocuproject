#pragma once

#include "eGrassType.h"

namespace lab5
{
	class Lawn//잔디'밭'
	{
	public:
		Lawn();
		virtual ~Lawn(); //가상소멸자는 생성자나 함수에 붙이는 것으로, 자바처럼 실체를 가리키게 하는 역할

		virtual unsigned int GetArea() const = 0; //가상함수, 넓이 단위는 m2

		unsigned int GetGrassPrice(eGrassType grassType) const;
		unsigned int GetMinimumSodRollsCount() const; //sod는 잔디밭
	};
}