#pragma once
#include "RectangleLawn.h"

namespace lab5
{
	class SquareLawn : public RectangleLawn
	{
	public:
		SquareLawn();
		SquareLawn(unsigned int width);
		virtual ~SquareLawn();

		unsigned int GetArea() const;
	private:
		unsigned int mSqWidth;
	};
}