#include "SquareLawn.h"

namespace lab5
{
	SquareLawn::SquareLawn()
		: RectangleLawn()
	{
	}

	SquareLawn::SquareLawn(unsigned int width)
		: RectangleLawn(width)
		, mSqWidth(width)
	{

	}

	SquareLawn::~SquareLawn()
	{

	}

	unsigned int SquareLawn::GetArea() const
	{
		double area;
		area = mSqWidth * mSqWidth;
		int result = static_cast<int>(area + 0.9); //넓이 내림으로
		return result;
	}

	/*
	unsigned int RectangleLawn::GetMinimumFencesCount() const
	{
		//둘레의 길이를 내림한 다음에 0.25로 나눴음
		double cirCumference = RectangleLawn::GetSqWidth() * 4;
		unsigned int fenceCount = static_cast<unsigned int>(cirCumference);
		double realFenceCount = fenceCount / 0.25;
		unsigned int result = static_cast<unsigned int>(realFenceCount + 0.9); //최종 펜스의 개수는 올림씀
		return result;
	}

	unsigned int RectangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		unsigned int fenceCount = RectangleLawn::GetSqWidth() * 4;

		switch (fenceType)
		{
			unsigned int fencePrice;
		case RED_CEDAR:
			fencePrice = fenceCount * 6;
			return fencePrice;

		case SPRUCE:
			fencePrice = fenceCount * 7;
			return fencePrice;
		default:
			return 0;
		}

		return 0;
	}
	*/
}