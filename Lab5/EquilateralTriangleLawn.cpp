#include "EquilateralTriangleLawn.h"

namespace lab5
{
	EquilateralTriangleLawn::EquilateralTriangleLawn()
		: Lawn()
	{
	}

	EquilateralTriangleLawn::EquilateralTriangleLawn(unsigned int width)
		: Lawn()
		, mEqWidth(width)
	{

	}

	EquilateralTriangleLawn::~EquilateralTriangleLawn()
	{

	}

	unsigned int EquilateralTriangleLawn::GetArea() const
	{
		double area = ((mEqWidth * mEqWidth) * sqrt(3.f)) / 4.f;
		unsigned int result = static_cast<unsigned int>(area); //���� ��������
		return result;
	}

	//�潺�� �˳��ؾ� �ϴ� �� �ø�
	unsigned int EquilateralTriangleLawn::GetMinimumFencesCount() const
	{
		double fenceCount = mEqWidth * 3 / 0.25;
		unsigned int result = static_cast<unsigned int>(fenceCount + 0.9);
		return result;
	}

	unsigned int EquilateralTriangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		unsigned int fenceCount = mEqWidth * 3;
		unsigned int fencePrice;

		switch (fenceType)
		{
			
		case RED_CEDAR:
			fencePrice = fenceCount * 6;
			return fencePrice;

		case SPRUCE:
			fencePrice = fenceCount * 7;
			return fencePrice;
		default:
			return 0;
		}
		return 0;
	}
}