#include "CircleLawn.h"

//CircleLawn에는 울타리 못침
namespace lab5
{
	CircleLawn::CircleLawn()
		: Lawn()
	{
	}

	CircleLawn::CircleLawn(unsigned int radius)
		: Lawn()
		, mRadius(radius)
	{
	}

	CircleLawn::~CircleLawn()
	{
	}

	unsigned int CircleLawn::GetArea() const
	{
		double area = mRadius * mRadius * PI;
		int result = static_cast<unsigned int>(area + 0.9); //넓이 내림으로
		return result;
	}
}

/*
unsigned int CircleLawn::GetMinimumFencesCount() const
	{
		//둘레의 길이를 내림한 다음에 0.25로 나눴음
		double cirCumference = (mRadius * 2) * PI;
		unsigned int fenceCount = static_cast<unsigned int>(cirCumference);
		double realFenceCount = fenceCount / 0.25;
		unsigned int result = static_cast<unsigned int>(realFenceCount + 0.9); //최종 펜스의 개수는 올림씀
		return result;
	}

	unsigned int CircleLawn::GetFencePrice(eFenceType fenceType) const
	{
		double circumference = (mRadius * 2) * PI;
		unsigned int fenceCount = static_cast<unsigned int>(circumference);

		switch (fenceType)
		{
			unsigned int fencePrice;
		case RED_CEDAR:
			fencePrice = fenceCount * 6;
			return fencePrice;

		case SPRUCE:
			fencePrice = fenceCount * 7;
			return fencePrice;
		default:
			return -1;
		}

		return -1;
	}
	*/