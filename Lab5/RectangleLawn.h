#pragma once
#include "IFenceable.h"
#include "Lawn.h"

namespace lab5
{
	class RectangleLawn : public IFenceable, public Lawn
	{
	public:
		RectangleLawn();
		RectangleLawn(unsigned int width, unsigned int height);
		RectangleLawn(unsigned int width);
		virtual ~RectangleLawn();

		virtual unsigned int GetArea() const;
		virtual unsigned int GetMinimumFencesCount() const;
		virtual unsigned int GetFencePrice(eFenceType fenceType) const;

	private:
		unsigned int* mWidth = new unsigned int[2];
	};
}