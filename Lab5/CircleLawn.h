#pragma once
#include "IFenceable.h"
#include "Lawn.h"

namespace lab5
{
	class CircleLawn : public Lawn
	{
	public:
		CircleLawn();
		CircleLawn(unsigned int radius);
		virtual ~CircleLawn();

		virtual unsigned int GetArea() const;

	private:
		unsigned int mRadius;
		const double PI = 3.14;
	};
}