#pragma once

namespace lab5
{
	enum eGrassType
	{
		//잔디 1개 넓이 = 0.3m2
		BERMUDA, //버뮤다 잔디(잔디나 목초용 풀) - 800cents/m2
		BAHIA, //바이아 잔디(열대 아메리카 원산지 풀, 줄기높이 3~90cm) - 500cents/m2
		BENTGRASS, //겨이삭띠 - 300cents/m2
		PERENNIAL_RYEGRASS, //다년생 라이 그래스(가는 보리풀, 목초) - 250cents/m2
		ST_AUGUSTINE //세인트 오거스틴. 미국산 다년초. 지표면 보호용 - 450cents/m2
	};
}