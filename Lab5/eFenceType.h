#pragma once

namespace lab5
{
	enum eFenceType //울타리 자재 종류
	{
		//1개당 폭 25cm
		RED_CEDAR, //연필향나무 - $6/m
		SPRUCE //가문비나무 - $7/m
	};
}