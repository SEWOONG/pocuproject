#include "Lawn.h"

namespace lab5
{
	Lawn::Lawn()
	{
	}

	Lawn::~Lawn()
	{
	}

	unsigned int Lawn::GetGrassPrice(eGrassType grassType) const
	{
		double middleResult;
		unsigned int result;

		//면적보다 풀의 크기를 작게 함
		switch (grassType)
		{
		case BERMUDA:
			result = GetArea() * 8;
			return result;

		case BAHIA:
			result = GetArea() * 5;
			return result;

		case BENTGRASS:
			result = GetArea() * 3;
			return result;

		case PERENNIAL_RYEGRASS:
			//가격 올림
			middleResult = GetArea() * 2.5;
			result = static_cast<unsigned int>(middleResult + 0.9);
			return result;

		case ST_AUGUSTINE:
			middleResult = GetArea() * 4.5;
			result = static_cast<unsigned int>(middleResult + 0.9);
			return result;

		default:
			return 0;
		}
		return 0;
	}

	//개수는 올림으로 함.
	unsigned int Lawn::GetMinimumSodRollsCount() const
	{
		double sodRoll = 0.3f;
		double needRollCount = GetArea() / sodRoll;
		int result = static_cast<int>(needRollCount + 0.9);

		return result;
	}
}
/*
double occupyArea;
unsigned int toIntOccupyArea;
unsigned int result;
		case BERMUDA:
			occupyArea = GetArea() / 100.f;
			toIntOccupyArea = static_cast<int>(occupyArea);
			result = toIntOccupyArea * 800;
			return result;
			*/