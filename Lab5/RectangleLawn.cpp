#include "RectangleLawn.h"

namespace lab5
{
	RectangleLawn::RectangleLawn()
		: Lawn()
	{
		mWidth[0] = 0;
		mWidth[1] = 0;
	}

	RectangleLawn::RectangleLawn(unsigned int width, unsigned int height)
		: Lawn()
	{
		mWidth[0] = width;
		mWidth[1] = height;
	}

	RectangleLawn::RectangleLawn(unsigned int width)
	{
		mWidth[0] = width;
		mWidth[1] = 0;
	}

	RectangleLawn::~RectangleLawn()
	{
		delete[] mWidth;
	}

	unsigned int RectangleLawn::GetArea() const
	{
		double area;
		
		if (mWidth[1] != 0)
		{
			area = mWidth[0] * mWidth[1];
		}
		else
		{
			area = mWidth[0] * mWidth[0];
		}
		unsigned int result = static_cast<unsigned int>(area + 0.9); //���� ��������
		return result;
	}

	unsigned int RectangleLawn::GetMinimumFencesCount() const
	{
		double fenceCount;
		if (mWidth[1] != 0)
		{
			fenceCount = (mWidth[0] + mWidth[1] + mWidth[0] + mWidth[1]) / 0.25;
		}
		else
		{
			fenceCount = (mWidth[0] * 4) / 0.25;
		}
		unsigned int result = static_cast<unsigned int>(fenceCount + 0.9);
		return result;
	}

	unsigned int RectangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		unsigned int fenceCount;

		if (mWidth[1] != 0)
		{
			fenceCount = (mWidth[0] + mWidth[1] + mWidth[0] + mWidth[1]);
		}
		else
		{
			fenceCount = (mWidth[0] * 4);
		}

		switch (fenceType)
		{
			unsigned int fencePrice;

		case RED_CEDAR:
			fencePrice = fenceCount * 6;
			return fencePrice;

		case SPRUCE:
			fencePrice = fenceCount * 7;
			return fencePrice;
		default:
			return 0;
		}

		return 0;
	}
}