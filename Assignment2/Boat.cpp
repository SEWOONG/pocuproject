#include "Boat.h"

namespace assignment2
{
	Boat::Boat()
		: Vehicle()
	{
	}

	Boat::Boat(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		SetType(1);
	}

	Boat::~Boat()
	{
	}

	unsigned int Boat::GetSailSpeed()
	{
		unsigned int ex1, ex2;

		ex1 = 800 - (10 * PassengersWeight());
		ex2 = 20;
		
		if (ex1 > ex2)
		{
			return ex1;
		}
		else
		{
			return ex2;
		}

		return 0;
	}

	unsigned int Boat::GetMaxSpeed()
	{
		return GetSailSpeed();
	}

	Boatplane Boat::operator+(Airplane& plane)
	{
		Boatplane bp(Vehicle::GetMaxPassengersCount() + plane.GetMaxPassengersCount());
		
		unsigned int i, j;
		for (i = 0; i <= Vehicle::GetPassengersCount() * 2; i++)
		{
			bp.AddPassenger(Vehicle::GetPassenger(0));
			RemovePassenger(0);
		}

		for (j = i; j <= plane.GetPassengersCount() * 2; j++)
		{
			bp.AddPassenger(plane.GetPassenger(j));
			plane.RemovePassenger(j);
		}

		return bp;
	}
}