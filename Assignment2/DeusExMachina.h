#pragma once

#include "Vehicle.h"

namespace assignment2
{
	class Vehicle;

	class DeusExMachina
	{

	public:
		DeusExMachina();
		~DeusExMachina();
		static DeusExMachina* GetInstance();
		void Travel() const;
		bool AddVehicle(Vehicle* vehicle);
		bool RemoveVehicle(unsigned int i);
		const Vehicle* GetFurthestTravelled() const;

		
	private:
		unsigned int mCurrentCount;
		unsigned int mMaxCount;
		unsigned int mTotal[10];
		unsigned int mFinal[10];
		unsigned int mType[10];
		unsigned int mTravelCount;
	};
}