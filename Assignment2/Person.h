#pragma once

#include <string>

namespace assignment2
{
	class Person
	{
	public:
		Person();
		Person(const Person& p);
		Person(const char* name, unsigned int weight);
		~Person();

		const std::string& GetName() const;
		unsigned int GetWeight() const;
		void SetName(std::string s);
		void SetWeight(unsigned int w);

	private:
		Person* mPerson;
		std::string mName;
		unsigned int mWeight;
	};
}