#pragma once

#include <cmath>

#include "Vehicle.h"
#include "IDrivable.h"

namespace assignment2
{
	class Motorcycle : public Vehicle, public IDrivable
	{
	public:
		Motorcycle();
		~Motorcycle();

		virtual unsigned int GetDriveSpeed();
		virtual unsigned int GetMaxSpeed();
	};
}