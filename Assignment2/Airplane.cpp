#include "Airplane.h"
#include "Boat.h"
#include "Boatplane.h"

namespace assignment2
{
	Airplane::Airplane()
		: Vehicle()
	{
	}

	Airplane::Airplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		SetType(0);
	}

	Airplane::~Airplane()
	{
	}

	unsigned int Airplane::GetDriveSpeed()
	{
		int pw = PassengersWeight();
		const double E = 2.71828182845904523536f;
		double speed = 4.f * pow(E, ((-pw + 400.f) / 70.f));
		unsigned int maxSpeed = static_cast<unsigned int>(speed + 0.5); //속도 반올림함

		return maxSpeed;
	}

	unsigned int Airplane::GetFlySpeed()
	{
		int pw = PassengersWeight();
		const double E = 2.71828182845904523536f;
		double speed = 200.f * (pow(E, (800.f - pw) / 500.f));
		unsigned int maxSpeed = static_cast<unsigned int>(speed + 0.5); //속도 반올림함
		return maxSpeed;
	}

	unsigned int Airplane::GetMaxSpeed()
	{
		if (GetDriveSpeed() > GetFlySpeed())
		{
			return GetDriveSpeed();
		}
		else
		{
			return GetFlySpeed();
		}

		return 0;
	}

	Boatplane Airplane::operator+(Boat& boat)
	{
		Boatplane bp(Vehicle::GetMaxPassengersCount() + boat.GetMaxPassengersCount());

		unsigned int i, j;
		for (i = 0; i <= Vehicle::GetPassengersCount() * 2; i++)
		{
			bp.AddPassenger(Vehicle::GetPassenger(0));
			RemovePassenger(0);
		}
		

		for (j = 0; j <= boat.GetPassengersCount() * 2; j++)
		{
			bp.AddPassenger(boat.GetPassenger(0));
			boat.RemovePassenger(0);
		}

		return bp;
	}
}