#include "UBoat.h"

namespace assignment2
{
	UBoat::UBoat()
		: Vehicle(50)
	{
		SetType(6);
	}

	UBoat::~UBoat()
	{
	}

	unsigned int UBoat::GetDiveSpeed()
	{
		double speed = (500 * (log((Vehicle::PassengersWeight() + 150) / 150))) + 30;
		unsigned int diveSpeed = static_cast<unsigned int>(speed + 0.5);

		return diveSpeed;
	}

	unsigned int UBoat::GetSailSpeed()
	{
		unsigned int ex1, ex2;

		ex1 = 550 - (Vehicle::PassengersWeight() / 10);
		ex2 = 200;

		if (ex1 > ex2)
		{
			return ex1;
		}
		else
		{
			return ex2;
		}

		return 0;
	}

	unsigned int UBoat::GetMaxSpeed()
	{
		if (GetDiveSpeed() > GetSailSpeed())
		{
			return GetDiveSpeed();
		}
		else
		{
			return GetSailSpeed();
		}

		return 0;
	}
}