#pragma once

#include "Vehicle.h"
#include "IFlyable.h"
#include "ISailable.h"

namespace assignment2
{
	class Boatplane : public Vehicle, public IFlyable, public ISailable
	{
	public:
		Boatplane();
		Boatplane(unsigned int maxPassengersCount);
		~Boatplane();

		virtual unsigned int GetFlySpeed();
		virtual unsigned int GetSailSpeed();
		virtual unsigned int GetMaxSpeed();
	};
}