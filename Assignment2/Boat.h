#pragma once

#include "Boatplane.h"
#include "Vehicle.h"
#include "ISailable.h"
#include "Airplane.h"

namespace assignment2
{
	class Airplane;
	class Boatplane;

	class Boat : public Vehicle, public ISailable
	{
	public:
		Boat();
		Boat(unsigned int maxPassengersCount);
		~Boat();

		virtual unsigned int GetSailSpeed();
		virtual unsigned int GetMaxSpeed();
		Boatplane operator+(Airplane& plane);
	};
}