#pragma once

#include "Boat.h"
#include "Boatplane.h"
#include "Vehicle.h"
#include "IDrivable.h"
#include "IFlyable.h"

namespace assignment2
{
	class Boat;
	class Boatplane;

	class Airplane : public Vehicle, public IFlyable, public IDrivable
	{
	public:
		Airplane();
		Airplane(unsigned int maxPassengersCount);
		~Airplane();

		virtual unsigned GetDriveSpeed();
		virtual unsigned int GetFlySpeed();
		virtual unsigned int GetMaxSpeed();
		Boatplane operator+(Boat& boat);
	};
}