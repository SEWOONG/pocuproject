#pragma once

#include "Vehicle.h"
#include "Trailer.h"
#include "IDrivable.h"

namespace assignment2
{
	class Trailer;

	class Sedan : public Vehicle, public IDrivable
	{
	public:
		Sedan();
		~Sedan();

		virtual unsigned int GetDriveSpeed();
		virtual unsigned int GetMaxSpeed();

		bool AddTrailer(const Trailer* trailer);
		bool RemoveTrailer();

	private:
		unsigned int mTrailerWeight;
	};
}