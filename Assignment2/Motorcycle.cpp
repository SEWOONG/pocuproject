#include "Motorcycle.h"

namespace assignment2
{
	Motorcycle::Motorcycle()
		: Vehicle(2)
	{
		SetType(3);
	}

	Motorcycle::~Motorcycle()
	{
	}

	unsigned int Motorcycle::GetDriveSpeed()
	{
		double speed = pow(Vehicle::PassengersWeight() / 15.f, 3) + (2.f * Vehicle::PassengersWeight()) + 400.f;
		unsigned int castSpeed = static_cast<unsigned int>(speed + 0.5); //속도 반올림함
		unsigned int minSpeed = 0;
		
		if (castSpeed > minSpeed)
		{
			return castSpeed;
		}
		else
		{
			return minSpeed;
		}

		return 0;
	}

	unsigned int Motorcycle::GetMaxSpeed()
	{
		return GetDriveSpeed();
	}
}