#pragma once

#include "Vehicle.h"
#include "IDivable.h"
#include "ISailable.h"

namespace assignment2
{
	class UBoat : public Vehicle, public IDivable, public ISailable
	{
	public:
		UBoat();
		~UBoat();

		virtual unsigned int GetDiveSpeed();
		virtual unsigned int GetSailSpeed();
		virtual unsigned int GetMaxSpeed();

	private:
		unsigned int mMaxPassengers;
	};
}