#include "Boatplane.h"

namespace assignment2
{
	Boatplane::Boatplane()
		: Vehicle(0)
	{
	}

	Boatplane::Boatplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
		SetType(2);
	}

	Boatplane::~Boatplane()
	{
	}

	unsigned int Boatplane::GetFlySpeed()
	{
		int pw = PassengersWeight();
		const double E = 2.71828182845904523536f;
		double speed = 150.f * pow(E, ((-pw + 500) / 300));
		unsigned int maxSpeed = static_cast<unsigned int>(speed + 0.5); //속도 반올림함
		
		return maxSpeed;
	}

	unsigned int Boatplane::GetSailSpeed()
	{
		unsigned int ex1, ex2;

		ex1 = static_cast<unsigned int>((800 - (1.7 * PassengersWeight())) + 0.5); //속도 반올림으로 함
		ex2 = 20;

		if (ex1 > ex2)
		{
			return ex1;
		}
		else
		{
			return ex2;
		}

		return 0;
	}

	unsigned int Boatplane::GetMaxSpeed()
	{
		if (GetSailSpeed() > GetFlySpeed())
		{
			return GetSailSpeed();
		}
		else
		{
			return GetFlySpeed();
		}

	}
}