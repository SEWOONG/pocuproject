#include "Person.h"

namespace assignment2
{
	Person::Person()
	{
	}

	Person::Person(const Person& p)
		: mWeight(p.GetWeight())
		, mName(p.GetName())
	{
	}

	Person::Person(const char* name, unsigned int weight)
		: mWeight(weight)
		, mName(name)
	{
	}

	Person::~Person()
	{
	}

	void Person::SetName(std::string s)
	{
		mName = s;
	}

	void Person::SetWeight(unsigned int w)
	{
		mWeight = w;
	}

	const std::string& Person::GetName() const
	{
		return mName;
	}

	unsigned int Person::GetWeight() const
	{
		return mWeight;
	}

}