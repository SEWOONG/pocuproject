#include "DeusExMachina.h"

namespace assignment2
{
	DeusExMachina::DeusExMachina()
		: mCurrentCount(0)
		, mMaxCount(10)
		, mTravelCount(0)
	{
	}

	DeusExMachina::~DeusExMachina()
	{
	}

	DeusExMachina* DeusExMachina::GetInstance()
	{
		static DeusExMachina* mDEM = new DeusExMachina;

		return mDEM;
	}

	void DeusExMachina::Travel() const
	{
		
	}

	bool DeusExMachina::AddVehicle(Vehicle* vehicle)
	{
		if (mCurrentCount >= 10)
		{
			return false;
		}

		mTotal[mCurrentCount] = vehicle->GetMaxSpeed();
		mType[mCurrentCount++] = vehicle->GetType();

		return true;
	}

	bool DeusExMachina::RemoveVehicle(unsigned int i)
	{
		if (i >= mCurrentCount)
		{
			return false;
		}

		for (unsigned int j = i; j < mMaxCount; j++)
		{
			mTotal[j] = mTotal[j + 1];
			mFinal[j] = mFinal[j + 1];
		}

		return true;
	}

	const Vehicle* DeusExMachina::GetFurthestTravelled() const
	{

		return NULL;
	}
}