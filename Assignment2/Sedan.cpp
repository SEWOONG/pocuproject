#include "Sedan.h"

namespace assignment2
{
	Sedan::Sedan()
		: Vehicle(4)
		, mTrailerWeight(0)
	{
		SetType(4);
	}

	Sedan::~Sedan()
	{
	}

	unsigned int Sedan::GetDriveSpeed()
	{
		unsigned int x = Vehicle::PassengersWeight() + mTrailerWeight;
		unsigned speed;

		if (x <= 80)
		{
			speed = 480;
			return speed;
		}
		else if (x > 80)
		{
			speed = 458;
			return speed;
		}
		else if (x > 160)
		{
			speed = 400;
			return speed;
		}
		else if (x > 260)
		{
			speed = 380;
			return speed;
		}
		else if (x > 350)
		{
			speed = 300;
			return speed;
		}
		else
		{
			return 0;
		}
	}

	unsigned int Sedan::GetMaxSpeed()
	{
		return GetDriveSpeed();
	}

	bool Sedan::AddTrailer(const Trailer* trailer)
	{
		if (mTrailerWeight != 0)
		{
			return false;
		}
		SetType(5);
		mTrailerWeight += trailer->GetWeight();
		return true;
	}

	bool Sedan::RemoveTrailer()
	{
		if (mTrailerWeight == 0)
		{
			return false;
		}

		mTrailerWeight = 0;
		return true;
	}
}