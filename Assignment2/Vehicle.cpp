#include "Vehicle.h"

namespace assignment2
{
	Vehicle::Vehicle()
		: Person()
	{
	}

	Vehicle::Vehicle(unsigned int maxPassengersCount)
		: Person()
		, mMaxPassengersCount(maxPassengersCount)
		, mCurrentPassengersCount(0)
	{
		mPassengers = new Person[10];
	}

	Vehicle::~Vehicle()
	{
		delete[] mPassengers;
	}

	void Vehicle::SetMaxPassengersCount(unsigned int count)
	{
		mMaxPassengersCount = count;
	}

	unsigned int Vehicle::PassengersWeight()
	{
		unsigned int passengersWeight = 0;

		for (unsigned int i = 0; i < mCurrentPassengersCount; i++)
		{
			passengersWeight += mPassengers[i].GetWeight();
		}

		return passengersWeight;
	}

	bool Vehicle::AddPassenger(const Person* person)
	{
		if (mCurrentPassengersCount >= mMaxPassengersCount)
		{
			return false;
		}
		mPassengers[mCurrentPassengersCount++] = *person;
		return true;
	}

	bool Vehicle::RemovePassenger(unsigned int i)
	{
		if (i >= mMaxPassengersCount || i < 0)
		{
			return false;
		}
		Person* dp = &mPassengers[i];

		for (unsigned int j = i; j < mCurrentPassengersCount; j++)
		{
			mPassengers[j] = mPassengers[j + 1];
		}

		mCurrentPassengersCount--;

		return true;
	}

	unsigned int Vehicle::GetPassengersCount() const
	{
		return mCurrentPassengersCount;
	}

	unsigned int Vehicle::GetMaxPassengersCount() const
	{
		return mMaxPassengersCount;
	}

	const Person* Vehicle::GetPassenger(unsigned int i) const
	{
		return &mPassengers[i];
	}

	void Vehicle::SetType(unsigned int t)
	{
		mT = t;
	}

	unsigned int Vehicle::GetType() const
	{
		return mT;
	}

}