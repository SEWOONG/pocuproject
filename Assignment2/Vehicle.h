#pragma once

#include <cmath>
#include "DeusExMachina.h"
#include "Person.h"

namespace assignment2
{
	class Vehicle : public Person
	{
	public:
		Vehicle();
		Vehicle(unsigned int maxPassengersCount);
		~Vehicle();

		virtual unsigned int GetMaxSpeed() = 0;
		
		void SetMaxPassengersCount(unsigned int count);
		unsigned int PassengersWeight();
		bool AddPassenger(const Person* person);
		bool RemovePassenger(unsigned int i);
		const Person* GetPassenger(unsigned int i) const;
		unsigned int GetPassengersCount() const;
		unsigned int GetMaxPassengersCount() const;

		void SetType(unsigned int t);
		unsigned int GetType() const;

	private:
		Person* mPassengers;
		Person* m;
		unsigned int mMaxPassengersCount;
		unsigned int mCurrentPassengersCount;
		unsigned int mT;

	};
}