#include <cassert>
#include <limits>
#include "SmartStack.h"
#include "SmartQueue.h"
#include "QueueStack.h"
#include <iostream>
using namespace std;

void SmartStackTest()
{
	// {}
	assignment3::SmartStack<float> smartStack;
	assert(smartStack.Count() == 0);
	assert(smartStack.Max() == std::numeric_limits<float>::lowest());
	assert(smartStack.Min() == std::numeric_limits<float>::max());

	// {10.1}
	smartStack.Push(10.1f);
	assert(smartStack.Count() == 1);
	assert(smartStack.Max() == 10.1f);
	assert(smartStack.Min() == 10.1f);
	assert(smartStack.Sum() == 10.1f);
	assert(smartStack.Average() == 10.1);
	assert(smartStack.Variance() == 0.0);
	assert(smartStack.StandardDeviation() == 0.0f);

	// {10.1, -5.5, 10.1, -5.5, 20.2, -11.0}
	smartStack.Push(-5.5f);
	smartStack.Push(10.1f);
	smartStack.Push(-5.5f);
	smartStack.Push(20.2f);
	smartStack.Push(-11.0);
	assert(smartStack.Count() == 6);
	assert(smartStack.Max() == 20.2f);
	assert(smartStack.Min() == -11.0f);
	assert(smartStack.Sum() == 18.4f);
	assert(smartStack.Average() == 3.067);
	assert(smartStack.Variance() == 122.856);
	assert(smartStack.StandardDeviation() == 11.084);

	// {10.1, -5.5, 10.1, -5.5, 20.2}
	assert(smartStack.Peek() == -11.0f);
	float popped = smartStack.Pop();
	assert(popped == -11.f);
	assert(smartStack.Count() == 5);
	assert(smartStack.Max() == 20.2f);
	assert(smartStack.Min() == -5.5f);
	assert(smartStack.Sum() == 29.4f);
	assert(smartStack.Average() == 5.88);
	assert(smartStack.Variance() == 99.938);
	assert(smartStack.StandardDeviation() == 9.997);

	// {10.1, -5.5, 10.1, -5.5}
	popped = smartStack.Pop();
	assert(popped == 20.2f);
	assert(smartStack.Count() == 4);
	assert(smartStack.Max() == 10.1f);
	assert(smartStack.Min() == -5.5f);
	assert(smartStack.Sum() == 9.2f);
	assert(smartStack.Average() == 2.3);
	assert(smartStack.Variance() == 60.84);
	assert(smartStack.StandardDeviation() == 7.8);

	// {10.1, -5.5, 10.1}
	popped = smartStack.Pop();
	assert(popped == -5.5f);
	assert(smartStack.Count() == 3);
	assert(smartStack.Max() == 10.1f);
	assert(smartStack.Min() == -5.5f);
	assert(smartStack.Sum() == 14.7f);
	assert(smartStack.Average() == 4.9);
	assert(smartStack.Variance() == 54.08);
	assert(smartStack.StandardDeviation() == 7.354);

	// {10.1, -5.5}
	popped = smartStack.Pop();
	assert(popped == 10.1f);
	assert(smartStack.Count() == 2);
	assert(smartStack.Max() == 10.1f);
	assert(smartStack.Min() == -5.5f);
	assert(smartStack.Sum() == 4.6f);
	assert(smartStack.Average() == 2.3);
	assert(smartStack.Variance() == 60.84);
	assert(smartStack.StandardDeviation() == 7.8);

	// {10.1}
	popped = smartStack.Pop();
	assert(popped == -5.5f);
	assert(smartStack.Count() == 1);
	assert(smartStack.Max() == 10.1f);
	assert(smartStack.Min() == 10.1f);
	assert(smartStack.Sum() == 10.1f);
	assert(smartStack.Average() == 10.1);
	assert(smartStack.Variance() == 0.0);
	assert(smartStack.StandardDeviation() == 0.0);

	// {}
	popped = smartStack.Pop();
	assert(popped == 10.1f);
	assert(smartStack.Count() == 0);
	assert(smartStack.Max() == std::numeric_limits<float>::lowest());
	assert(smartStack.Min() == std::numeric_limits<float>::max());
}

void SmartQueueTest()
{
	const float EPSILON = std::numeric_limits<float>::min();

	// {}
	assignment3::SmartQueue<float> smartQueue;
	assert(smartQueue.Count() == 0);
	assert(smartQueue.Max() == std::numeric_limits<float>::lowest());
	assert(smartQueue.Min() == std::numeric_limits<float>::max());

	// {20.2, -11.0, 10.1, -5.5, 10.1, -5.5}
	smartQueue.Enqueue(20.2f);
	smartQueue.Enqueue(-11.0f);
	smartQueue.Enqueue(10.1f);
	smartQueue.Enqueue(-5.5f);
	smartQueue.Enqueue(10.1f);
	smartQueue.Enqueue(-5.5f);
	assert(smartQueue.Peek() == 20.2f);
	assert(smartQueue.Count() == 6);
	assert(smartQueue.Max() == 20.2f);
	assert(smartQueue.Min() == -11.0f);
	assert(smartQueue.Sum() == 18.4f);
	assert(smartQueue.Average() == 3.067);
	assert(smartQueue.Variance() == 122.856);
	assert(smartQueue.StandardDeviation() == 11.084);

	// {-11.0, 10.1, -5.5, 10.1, -5.5} 
	float dequeued = smartQueue.Dequeue();
	assert(dequeued == 20.2f);
	assert(smartQueue.Count() == 5);
	assert(smartQueue.Max() == 10.1f);
	assert(smartQueue.Min() == -11.0f);
	assert((smartQueue.Sum() - (-1.8f)) < EPSILON);
	assert(smartQueue.Average() == -0.36);
	assert(smartQueue.Variance() == 76.974);
	assert(smartQueue.StandardDeviation() == 8.774);

	// {10.1, -5.5, 10.1, -5.5} 
	dequeued = smartQueue.Dequeue();
	assert(dequeued == -11.0f);
	assert(smartQueue.Count() == 4);
	assert(smartQueue.Max() == 10.1f);
	assert(smartQueue.Min() == -5.5f);
	assert(smartQueue.Sum() == 9.2f);
	assert(smartQueue.Average() == 2.3);
	assert(smartQueue.Variance() == 60.84);
	assert(smartQueue.StandardDeviation() == 7.8);

	// {-5.5, 10.1, -5.5} 
	dequeued = smartQueue.Dequeue();
	assert(dequeued == 10.1f);
	assert(smartQueue.Count() == 3);
	assert(smartQueue.Max() == 10.1f);
	assert(smartQueue.Min() == -5.5f);
	assert((smartQueue.Sum() - (-0.9f)) < EPSILON);
	assert(smartQueue.Average() == -0.3);
	assert(smartQueue.Variance() == 54.08);
	assert(smartQueue.StandardDeviation() == 7.354);

	// {10.1, -5.5} 
	dequeued = smartQueue.Dequeue();
	assert(dequeued == -5.5f);
	assert(smartQueue.Count() == 2);
	assert(smartQueue.Max() == 10.1f);
	assert(smartQueue.Min() == -5.5f);
	assert(smartQueue.Sum() == 4.6f);
	assert(smartQueue.Average() == 2.3);
	assert(smartQueue.Variance() == 60.84);
	assert(smartQueue.StandardDeviation() == 7.8);

	// {-5.5} 
	dequeued = smartQueue.Dequeue();
	assert(dequeued == 10.1f);
	assert(smartQueue.Count() == 1);
	assert(smartQueue.Max() == -5.5f);
	assert(smartQueue.Min() == -5.5f);
	assert(smartQueue.Sum() == -5.5f);
	assert(smartQueue.Average() == -5.5);
	assert(smartQueue.Variance() == 0.0);
	assert(smartQueue.StandardDeviation() == 0.0);

	// {} 
	dequeued = smartQueue.Dequeue();
	assert(dequeued == -5.5f);
	assert(smartQueue.Count() == 0);
	assert(smartQueue.Max() == std::numeric_limits<float>::lowest());
	assert(smartQueue.Min() == std::numeric_limits<float>::max());
}

void QueueStackTest()
{
	// {}
	assignment3::QueueStack<float> queueStack(3);
	assert(queueStack.Count() == 0);
	assert(queueStack.StackCount() == 0);
	assert(queueStack.Max() == std::numeric_limits<float>::lowest());
	assert(queueStack.Min() == std::numeric_limits<float>::max());

	// { {20, -15, 10}, {-5, 20, -15}, {10, -5} }
	queueStack.Enqueue(20.f);
	queueStack.Enqueue(-15.f);
	queueStack.Enqueue(10.f);
	queueStack.Enqueue(-5.f);
	queueStack.Enqueue(20.f);
	queueStack.Enqueue(-15.f);
	queueStack.Enqueue(10.f);
	queueStack.Enqueue(-5.f);
	assert(queueStack.Peek() == 10.f);
	assert(queueStack.Count() == 8);
	assert(queueStack.StackCount() == 3);
	assert(queueStack.Max() == 20.f);
	assert(queueStack.Min() == -15.f);
	assert(queueStack.Sum() == 20.f);
	assert(queueStack.Average() == 2.5);

	// { {20, -15}, {-5, 20, -15}, {10, -5} }
	float dequeued = queueStack.Dequeue();
	assert(dequeued = 10.f);
	assert(queueStack.Peek() == -15.f);
	assert(queueStack.Count() == 7);
	assert(queueStack.StackCount() == 3);
	assert(queueStack.Max() == 20.f);
	assert(queueStack.Min() == -15.f);
	assert(queueStack.Sum() == 10.f);
	assert(queueStack.Average() == 1.429);

	// { {20}, {-5, 20, -15}, {10, -5} }
	dequeued = queueStack.Dequeue();
	assert(dequeued = -15.f);
	assert(queueStack.Peek() == 20.f);
	assert(queueStack.Count() == 6);
	assert(queueStack.StackCount() == 3);
	assert(queueStack.Max() == 20.f);
	assert(queueStack.Min() == -15.f);
	assert(queueStack.Sum() == 25.f);
	assert(queueStack.Average() == 4.167);

	// { {-5, 20, -15}, {10, -5} }
	dequeued = queueStack.Dequeue();
	assert(dequeued = 20.f);
	assert(queueStack.Peek() == -15.f);
	assert(queueStack.Count() == 5);
	assert(queueStack.StackCount() == 2);
	assert(queueStack.Max() == 20.f);
	assert(queueStack.Min() == -15.f);
	assert(queueStack.Sum() == 5.f);
	assert(queueStack.Average() == 1.0);

	// { {-5, 20}, {10, -5} }
	dequeued = queueStack.Dequeue();
	assert(dequeued = -15.f);
	assert(queueStack.Peek() == 20.f);
	assert(queueStack.Count() == 4);
	assert(queueStack.StackCount() == 2);
	assert(queueStack.Max() == 20.f);
	assert(queueStack.Min() == -5.f);
	assert(queueStack.Sum() == 20.f);
	assert(queueStack.Average() == 5.0);

	// { {-5}, {10, -5} }
	dequeued = queueStack.Dequeue();
	assert(dequeued = 20.f);
	assert(queueStack.Peek() == -5.f);
	assert(queueStack.Count() == 3);
	assert(queueStack.StackCount() == 2);
	assert(queueStack.Max() == 10.f);
	assert(queueStack.Min() == -5.f);
	assert(queueStack.Sum() == 0.f);
	assert(queueStack.Average() == 0.0);

	// { {10, -5} }
	dequeued = queueStack.Dequeue();
	assert(dequeued = -5.f);
	assert(queueStack.Peek() == -5.f);
	assert(queueStack.Count() == 2);
	assert(queueStack.StackCount() == 1);
	assert(queueStack.Max() == 10.f);
	assert(queueStack.Min() == -5.f);
	assert(queueStack.Sum() == 5.f);
	assert(queueStack.Average() == 2.5);

	// { {10} }
	dequeued = queueStack.Dequeue();
	assert(dequeued = -5.f);
	assert(queueStack.Peek() == 10.f);
	assert(queueStack.Count() == 1);
	assert(queueStack.StackCount() == 1);
	assert(queueStack.Max() == 10.f);
	assert(queueStack.Min() == 10.f);
	assert(queueStack.Sum() == 10.f);
	assert(queueStack.Average() == 10.0);

	// {}
	dequeued = queueStack.Dequeue();
	assert(dequeued = 10.f);
	assert(queueStack.Count() == 0);
	assert(queueStack.StackCount() == 0);
	assert(queueStack.Max() == std::numeric_limits<float>::lowest());
	assert(queueStack.Min() == std::numeric_limits<float>::max());
}

int main()
{
	SmartStackTest();
	SmartQueueTest();
	QueueStackTest();

	return 0;
}