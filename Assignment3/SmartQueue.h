#pragma once
#include <queue>
#include <limits>
#include <cmath>

#include "SmartStack.h"

namespace assignment3
{
	template <typename T> //T
	class SmartQueue //SmartStack에서 Enqueue, Dequeue만 바꿈. Dequeue는 위험할 수 있음
	{
	public:
		SmartQueue()
			: mSum(0)
			, mMax(std::numeric_limits<T>::lowest())
			, mMin(std::numeric_limits<T>::max())
		{
		}

		virtual ~SmartQueue() = default;
		
		void Enqueue(const T& number)
		{
			if (number >= mMax)
			{
				mMax = number;
			}
			if (number <= mMin)
			{
				mMin = number;
			}

			mSum += number;
			mQueue.push(number);
		}

		T Peek() const
		{
			return mQueue.front();
		}

		T Dequeue()
		{
			T rData = mQueue.front();
			mSum -= rData;

			mQueue.pop();

			/* 데이터 삭제 시 Min과 Max값 재지정 */
			std::queue<T> copyQueue(mQueue);
			mMax = std::numeric_limits<T>::lowest();
			mMin = std::numeric_limits<T>::max();
			while (!(copyQueue.empty()))
			{
				if (copyQueue.front() >= mMax)
				{
					mMax = copyQueue.front();
				}
				if (copyQueue.front() <= mMin)
				{
					mMin = copyQueue.front();
				}
				copyQueue.pop();
			}

			return rData;
		}

		T Max() const
		{
			return mMax;
		}

		T Min() const
		{
			return mMin;
		}

		double Average() const
		{
			double average = 0;

			average = mSum / mQueue.size();
			return round(average * 1000) / 1000;
		}

		T Sum() const
		{
			return round(mSum * 1000) / 1000;
		}

		double Variance() const
		{
			double average;
			average = Average();

			double variance;
			double averageDifferenceSum = 0;
			std::queue<T> copyQueue(mQueue);

			T data;
			for (unsigned int i = 0; i < mQueue.size(); i++)
			{
				data = copyQueue.front();
				copyQueue.pop();
				if (average > data)
				{
					averageDifferenceSum += pow((average - static_cast<double>(data)), 2.0);
				}
				else
				{
					averageDifferenceSum += pow((data - average), 2);
				}
			}
			variance = averageDifferenceSum / (mQueue.size());
			return round(variance * 1000) / 1000;
		}

		double StandardDeviation() const
		{
			double standardDeviation;
			double variance = Variance();

			standardDeviation = sqrt(variance);
			standardDeviation = round(standardDeviation * 10000) / 10000;
			return round(standardDeviation * 1000) / 1000;
		}

		unsigned int Count() const
		{
			return mQueue.size();
		}

		SmartStack<T> mStack;

	private:
		std::queue<T> mQueue;
		T mMin;
		T mMax;
		T mSum;
	};
}