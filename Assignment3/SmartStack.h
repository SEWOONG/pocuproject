#pragma once
#include <limits>
#include <stack>
#include <cmath>
#include <typeinfo>
#include <iostream>
#include <algorithm>

namespace assignment3
{
	template <typename T> //T
	class SmartStack
	{
	public:
		SmartStack()
			: mMax(std::numeric_limits<T>::lowest())
			, mMin(std::numeric_limits<T>::max())
		{
		}

		virtual ~SmartStack() = default;
		
		void Push(const T& number)
		{
			if (number >= mMax)
			{
				mMax = number;
			}
			if (number <= mMin)
			{
				mMin = number;
			}

			mSmartStack.push(number);
		}

		T Pop()
		{
			T rData = mSmartStack.top();
			mSmartStack.pop();

			/* 데이터 삭제 시 Min과 Max값 재지정 */
			std::stack<T> copyStack(mSmartStack);
			mMax = std::numeric_limits<T>::lowest();
			mMin = std::numeric_limits<T>::max();
			while (!(copyStack.empty()))
			{
				if (copyStack.top() >= mMax)
				{
					mMax = copyStack.top();
				}
				if (copyStack.top() <= mMin)
				{
					mMin = copyStack.top();
				}
				copyStack.pop();
			}

			return rData;
		}

		T Peek() const
		{
			return mSmartStack.top();
		}

		T Max() const
		{
			return mMax;
		}

		T Min() const
		{
			return mMin;
		}

		double Average() const
		{
			double average = 0;

			average = Sum() / mSmartStack.size();
			return round(average * 1000) / 1000;
		}

		T Sum() const
		{
			T sum = 0;
			std::stack<T> copyStack(mSmartStack);
			while (!(copyStack.empty()))
			{
				sum += copyStack.top();
				copyStack.pop();
			}
			return round(sum * 1000) / 1000;
		}

		double Variance() const
		{
			double average;
			double variance;
			double averageDifferenceSum = 0;

			average = Sum() / mSmartStack.size();

			std::stack<T> copyStack(mSmartStack);
			T data;
			for (unsigned int i = 0; i < mSmartStack.size(); i++)
			{
				data = copyStack.top();
				copyStack.pop();
				averageDifferenceSum += pow((average - static_cast<double>(data)), 2.0);
			}
			variance = averageDifferenceSum / (mSmartStack.size());
			return round(variance * 1000) / 1000;
		}

		double StandardDeviation() const
		{
			double standardDeviation;
			double variance = Variance();

			standardDeviation = sqrt(variance);
			return round(standardDeviation * 1000) / 1000;
		}

		unsigned int Count() const
		{
			return mSmartStack.size();
		}

	private:
		std::stack<T> mSmartStack;
		T mMin;
		T mMax;
	};
}