#pragma once
#include "SmartStack.h"

namespace assignment3
{
	template <typename T> //기본
	class QueueStack
	{
	public:
		QueueStack(unsigned int maxStackSize)
			: mMaxStackSize(maxStackSize)
			, mMax(std::numeric_limits<T>::lowest())
			, mMin(std::numeric_limits<T>::max())
			, mSum(0)
			, mTotalSize(0)
		{
		}

		~QueueStack()
		{
		}

		void Enqueue(const T& number)
		{
			//큐 속의 스택이 정한 수 초과인지
			if (mQueue.mStack.Count() >= mMaxStackSize)
			{
				mQueue.Enqueue(0);
			}
			mSum += number;

			//값 전체 max와 min값 넣기 위함
			if (number >= mMax)
			{
				mMax = number;
			}
			if (number <= mMin)
			{
				mMin = number;
			}

			mQueue.mStack.Push(number);
			mTotalSize++;
		}

		T Peek() const
		{
			return mQueue.front().mStack;
		}

		T Dequeue()
		{
			T rData = mData[mCurrentFront].Pop();
			for (unsigned int i = mData[mCurrentFront].Count(); i < mTotalDataCount - 1; i++)
			{
				mQsValues[i] = mQsValues[i + 1];
			}
			mQsValues[mTotalDataCount - 1] = NULL;


			mMax = std::numeric_limits<T>::lowest();
			mMin = std::numeric_limits<T>::max();
			for (unsigned int i = 0; i < mTotalDataCount - 1; i++)
			{
				if (mMax < mQsValues[i])
				{
					mMax = mQsValues[i];
				}
				if (mMin > mQsValues[i])
				{
					mMin = mQsValues[i];
				}
			}
			mTotalDataCount--;

			if (mData[mCurrentFront].Count() == 0 && mCurrentFront < mCurrentCount)
			{
				mCurrentFront++;
			}

			return rData;
		}

		T Max() const
		{
			return mMax;
		}

		T Min() const
		{
			return mMin;
		}

		double Average() const
		{
			double sum = 0;
			double average = 0;
			for (unsigned int i = 0; i <= mCurrentCount; i++)
			{
				sum += mData[i].Sum();
			}
			average = sum / static_cast<double>(mTotalDataCount);
			average = round(average * 1000) / 1000;
			return average;
		}

		T Sum() const
		{
			T sum = 0;
			for (unsigned int i = mCurrentFront; i <= mCurrentCount; i++)
			{
				sum += mData[i].Sum();
			}
			return round(sum * 1000) / 1000;
		}

		unsigned int Count() const
		{
			return mTotalDataCount;
		}

		unsigned int StackCount() const
		{
			if (mTotalDataCount == 0)
			{
				return 0;
			}
			else
			{
				return mCurrentCount - mCurrentFront + 1;
			}

			return 0;
		}

	private:
		SmartQueue<T> mQueue;
		T mSum;
		T mMin;
		T mMax;
		unsigned int mMaxStackSize;
		unsigned int mTotalSize;

	};
	
}