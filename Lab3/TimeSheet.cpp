#include "Timesheet.h"
#include <iostream>

namespace lab3
{
	//근무시간 쓴 용지
	TimeSheet::TimeSheet(const char* name, unsigned int maxEntries)
		: M_MAX_ENTRIES(maxEntries)
		, mCharName(name)
	{
		mWorkInHour = new int[M_MAX_ENTRIES];
		mName = mCharName;
		mWorkIndex = 0;
		mSum = 0;
	}

	//소멸자
	TimeSheet::~TimeSheet()
	{
		delete[] mWorkInHour;
	}

	//용지에 한 시간 추가
	void TimeSheet::AddTime(int timeInHours)
	{
		if (mWorkIndex >= M_MAX_ENTRIES)
		{
			return;
		}
		if (timeInHours >= 1 && timeInHours <= 10)
		{
			mWorkInHour[mWorkIndex++] = timeInHours;
			mSum += timeInHours;
		}
	}

	//일 단위로 직원이 근무한 시간
	int TimeSheet::GetTimeEntry(unsigned int index) const
	{
		if (index >= 0 && index <= mWorkIndex-1)
		{
			return mWorkInHour[index];
		}
		else
		{
			return -1;
		}
		return -1;
	}

	//전체 단위로 직원들의 총 시간 
	int TimeSheet::GetTotalTime() const
	{
		return mSum;
	}

	//직원들의 하루 평균시간
	float TimeSheet::GetAverageTime() const
	{
		if (mSum == 0)
		{
			return 0.0f;
		}
		return (float)mSum / (float)mWorkIndex;
	}

	//직원의 하루 표준편차
	float TimeSheet::GetStandardDeviation() const
	{
		float avg = 0.0;
		float standardSum = 0.0;
		float valuePow = 0.0;

		avg = (float)mSum / (float)mWorkIndex;

		for (unsigned int i = 0; i < mWorkIndex; i++)
		{
			if (mWorkInHour[i] >= (int)avg)
			{
				valuePow = (float)mWorkInHour[i] - avg;
			}
			else
			{
				valuePow = avg - (float)mWorkInHour[i];
			}
			valuePow = pow(valuePow, 2);
			standardSum += valuePow;
		}

		return ((float)standardSum / (float)(mWorkIndex -1));
	}

	//근무기록 대상의 이름 반환
	const std::string& TimeSheet::GetName() const
	{
		return mName;
	}
}