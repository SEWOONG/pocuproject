#pragma once
#include <string>
#include <cmath>

namespace lab3
{
	class TimeSheet
	{
	public:
		TimeSheet(const char* name, unsigned int maxEntries);
		~TimeSheet();
		void AddTime(int timeInHours);
		int GetTimeEntry(unsigned int index) const;
		int GetTotalTime() const;
		float GetAverageTime() const;
		float GetStandardDeviation() const;
		const std::string& GetName() const;

	private:
		// 필요에 따라 private 변수와 메서드를 추가하세요.
		const char* mCharName;
		const unsigned int M_MAX_ENTRIES;
		std::string mName;
		int* mWorkInHour;
		unsigned int mWorkIndex;
		float mSum;
	};
}