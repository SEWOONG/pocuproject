#include "MyString.h"

using namespace std;

namespace assignment1
{
	MyString::MyString(const char* s)
		: mTotalLength(sizeof(mStr))
	{
		mStr = new char[mTotalLength];
		memcpy(mStr, s, mTotalLength);
	}

	MyString::MyString(const MyString& other)
		: mTotalLength(sizeof(other.mStr))
	{
		mStr = new char[mTotalLength];
		memcpy(mStr, other.mStr, mTotalLength);
	}

	MyString::~MyString()
	{
		delete[] mStr;
	}

	unsigned int MyString::GetLength() const
	{
		return mTotalLength;
	}

	const char* MyString::GetCString() const
	{
		return mStr;
	}

	void MyString::Append(const char* s)
	{
		mStr = mStr + (*s);
	}

	MyString MyString::operator+(const MyString& other) const
	{
		MyString sum(mStr);
		sum.mStr = mStr + *(other.mStr);
		return sum;
	}

	int MyString::IndexOf(const char* s)
	{
		for (int i = 0; i < sizeof(mStr); i++)
		{
			if (mStr[i] == s[0])
				return i;
		}
		return -1;
	}

	int MyString::LastIndexOf(const char* s)
	{
		for (int i = sizeof(mStr)-1; i >= 0; i++)
		{
			if (mStr[i] == s[0])
				return i;
		}
		return -1;
	}

	void MyString::Interleave(const char* s)
	{
		MyString my("");

		int i = 0;
		for (i; i < sizeof(mStr) && i < sizeof(s); i++)
		{
			char* strIdx;
			char* sIdx;
			
			strIdx += mStr[i];
			sIdx += s[i];

			my.Append(strIdx);
			my.Append(sIdx);
			delete strIdx;
			delete sIdx;
		}

		if (i < sizeof(mStr))
		{
			i++;
			while (i < sizeof(mStr))
			{
				char* strIdx;
				strIdx += mStr[i];

				my.Append(strIdx);

				i++;

				delete strIdx;
			}
		}
		else if (i < sizeof(s))
		{
			i++;
			while (i < sizeof(s))
			{
				char* sIdx;
				sIdx += sIdx[i];

				my.Append(sIdx);

				i++;
				
				delete sIdx;
			}
		}
		mStr = my.mStr;
		
		delete& my;
	}

	bool MyString::RemoveAt(unsigned int index)
	{
		if (mStr[index] <= mTotalLength)
		{
			MyString my("");
			for (int i = 0; i < mTotalLength - 1; i++)
			{
				if (i == index)
				{
					continue;
				}
				char* sIdx;
				sIdx += mStr[i];

				my.Append(sIdx);
			}
			mStr = my.mStr;

			delete& my;

			return true;
		}
		return false;
	}

	void MyString::PadLeft(unsigned int totalLength)
	{
		
	}

	void MyString::PadLeft(unsigned int totalLength, const char c)
	{
	}

	void MyString::PadRight(unsigned int totalLength)
	{
	}

	void MyString::PadRight(unsigned int totalLength, const char c)
	{
	}

	void MyString::Reverse()
	{
	}

	bool MyString::operator==(const MyString& rhs) const
	{
		return false;
	}

	void MyString::ToLower()
	{
	}

	void MyString::ToUpper()
	{
	}
}