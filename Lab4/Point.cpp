#include "Point.h"

namespace lab4
{
	Point::Point()
		: mX(0.0)
		, mY(0.0)
	{
	}

	Point::Point(float x, float y)
		: mX(x)
		, mY(y)
	{
	}

	Point::Point(const Point& other)
		: mX(other.mX)
		, mY(other.mY)
	{
	}

	Point::~Point()
	{
	}

	Point operator*(float operand, Point& rhs)
	{
		return Point(operand * rhs.mX, operand * rhs.mY);
	}
	
	void Point::SetX(float x)
	{
		mX = x;
	}

	void Point::SetY(float y)
	{
		mY = y;
	}

	Point Point::operator+(const Point& other) const
	{
		return Point(mX + other.mX, mY + other.mY);
	}

	Point Point::operator-(const Point& other) const
	{
		return Point(mX - other.mX, mY - other.mY);
	}

	float Point::Dot(const Point& other) const
	{
		float result = mX * other.mX + mY * other.mY;

		return result;
	}

	Point Point::operator*(float operand) const
	{
		return Point(operand * mX, operand * mY);
	}

	Point Point::operator=(const Point& other)
	{
		Point::SetX(other.mX);
		Point::SetY(other.mY);

		return Point(mX, mY);
	}

	float Point::GetX() const
	{
		return mX;
	}

	float Point::GetY() const
	{
		return mY;
	}
}