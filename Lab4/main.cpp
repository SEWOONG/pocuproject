#include <cassert>

#include "Point.h"
#include "PolyLine.h"

using namespace lab4;
using namespace std;

int main()
{
	Point p1(2.f, 3.f);
	Point p2(-1.f, 4.f);

	Point p3 = p1 + p2;

	cout << "������" << endl;
	assert(p3.GetX() == 1.f);
	assert(p3.GetY() == 7.f);

	Point p4 = p2 - p1;
	cout << "������2" << endl;
	assert(p4.GetX() == -3.f);
	assert(p4.GetY() == 1.f);

	float dotProduct = p1.Dot(p2);
	cout << "����" << endl;
	assert(dotProduct == 10.f);

	Point p5 = p1 * 5.f;
	cout << "��" << endl;
	assert(p5.GetX() == 10.f);
	assert(p5.GetY() == 15.f);

	Point p6 = 2.f * p2;
	cout << "��2" << endl;
	assert(p6.GetX() == -2.f);
	assert(p6.GetY() == 8.f);

	/* ----------------------- */

	PolyLine pl1;
	pl1.AddPoint(1.4f, 2.8f);
	pl1.AddPoint(3.7f, 2.5f);
	pl1.AddPoint(5.5f, 5.5f);
	pl1.AddPoint(-2.9f, 4.1f);
	pl1.AddPoint(4.3f, -1.0f);
	pl1.AddPoint(6.2f, 4.4f);
	pl1.AddPoint(6.2f, 4.4f);
	pl1.AddPoint(6.2f, 4.4f);
	pl1.AddPoint(6.2f, 4.4f);
	pl1.AddPoint(6.2f, 4.4f);

	cout << "����" << endl;
	bool bRemoved = pl1.RemovePoint(4);
	assert(bRemoved);
	cout << "����" << endl;

	Point minP(0.f, 0.f);
	Point maxP(0.f, 0.f);

	cout << "�̻��Ѱ�" << endl;
	pl1.TryGetMinBoundingRectangle(&minP, &maxP);
	assert(minP.GetX() == -2.9f);
	assert(minP.GetY() == 2.5f);
	assert(maxP.GetX() == 6.2f);
	assert(maxP.GetY() == 5.5f);

	return 0;
}