#pragma once
#include <iostream>

namespace lab4
{
	class Point
	{
		friend Point operator*(float operand, Point& rhs);
	public:
		Point();
		Point(float x, float y);
		Point(const Point& other);
		~Point();

		void SetX(const float x);
		void SetY(const float y);
		Point operator+(const Point& other) const;
		Point operator-(const Point& other) const;
		float Dot(const Point& other) const;
		Point operator*(float operand) const;
		Point operator=(const Point& other);

		float GetX() const;
		float GetY() const;
		
	private:
		float mX;
		float mY;
	};
}

