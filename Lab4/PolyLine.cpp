#include <cstring>
#include <cmath>
#include "PolyLine.h"

namespace lab4
{
	PolyLine::PolyLine()
	{
		mP = new Point[10];
		mIndex = 0;
	}

	PolyLine::PolyLine(const PolyLine& other)
	{
		mP = new Point[10];
		mIndex = 0;
	}

	PolyLine::~PolyLine()
	{
		delete[] mP;
	}

	bool PolyLine::AddPoint(float x, float y)
	{
		if (mIndex >= 10)
		{
			return false;
		}
		Point *dummy = new Point(x, y);
		mP[mIndex] = *dummy;
		mIndex++;

		return true;
	}

	bool PolyLine::AddPoint(const Point* point)
	{
		if (mIndex >= 10 || mIndex < 0)
		{
			return false;
		}

		mP[mIndex] = *point;
		mIndex++;

		return true;
	}

	bool PolyLine::RemovePoint(unsigned int i)
	{
		if (i >= 10 || i < 0)
		{
			return false;
		}
		if (mP[i].GetX() == 0.0 && mP[i].GetY() == 0.0)
		{
			return false;
		}

		Point* p = new Point(mP[i]);
		for (int j = i; j < mIndex; j++)
		{
			mP[j] = mP[j + 1];
		}
		
		delete p;
		mIndex--;
		return true;
	}

	bool PolyLine::TryGetMinBoundingRectangle(Point* outMin, Point* outMax) const
	{
		if (mIndex == 0)
		{
			return false;
		}

		float minX = 100000000;
		float minY = 100000000;
		float maxX = -100000000;
		float maxY = -100000000;

		for (int i = 0; i < mIndex; i++)
		{
			if (mP[i].GetX() < minX)
			{
				minX = mP[i].GetX();
			}
			if (mP[i].GetY() < minY)
			{
				minY = mP[i].GetY();
			}
			if (mP[i].GetX() > maxX)
			{
				maxX = mP[i].GetX();
			}
			if (mP[i].GetY() > maxY)
			{
				maxY = mP[i].GetY();
			}
		}
		Point oMin(minX, minY);
		Point oMax(maxX, maxY);
		*outMin = oMin;
		*outMax = oMax;

		delete &outMin;
		delete &outMax;
		return true;
	}

	const Point* PolyLine::operator[](unsigned int i) const
	{
		if (i >= 10 || i < 0)
		{
			return false;
		}
		return new Point(mP[i].GetX(), mP[i].GetY());
	}
}