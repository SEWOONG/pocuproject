#pragma once

#include <iostream>
#include <list>

#include "IceCube.h"

namespace lab9
{
	template <typename T>
	class ObjectPool
	{
	public:
		ObjectPool(size_t maxPoolSize);
		ObjectPool(const T& other) = delete;
		~ObjectPool();
		T* Get();
		void Return(T* t);
		size_t GetFreeObjectCount() const;
		size_t GetMaxFreeObjectCount() const;
		
		ObjectPool& operator=(const ObjectPool& other) = delete;
		
	private:
		size_t mMaxPoolSize;
		std::list<T*> mPoolObject;
	};

	template <typename T> ObjectPool<T>::ObjectPool(size_t maxPoolSize)
		: mMaxPoolSize(maxPoolSize)
	{
	}

	template <typename T> ObjectPool<T>::~ObjectPool()
	{
		for (auto it = mPoolObject.begin(); it != mPoolObject.end(); ++it)
		{
			delete (*it);
		}
		mPoolObject.clear();

	}

	template <typename T> T* ObjectPool<T>::Get()
	{

		if (mPoolObject.size() == 0)
		{
			std::unique_ptr<T> oneObject = std::make_unique<T>();
			return oneObject.get();
		}

		std::unique_ptr<T> oneObject = std::make_unique<T>(mPoolObject.front());
		mPoolObject.pop_front();
		return oneObject.get();
	}

	template <typename T> void ObjectPool<T>::Return(T* t)
	{
		if (mMaxPoolSize == mPoolObject.size())
		{
			delete t;
			return;
		}
		//delete t;
		mPoolObject.push_back(t);
	}

	template <typename T> size_t ObjectPool<T>::GetFreeObjectCount() const
	{
		return mPoolObject.size();
	}

	template <typename T> size_t ObjectPool<T>::GetMaxFreeObjectCount() const
	{
		return mMaxPoolSize;
	}
}