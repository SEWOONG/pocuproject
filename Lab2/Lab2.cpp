#include "Lab2.h"

namespace lab2
{
	void PrintIntegers(std::istream& in, std::ostream& out)
	{
		int number;

		out << std::setw(12) << std::setfill(' ') << "oct" << std::setw(11) << "dec" << std::setw(9) << "hex" << "\n";
		out << std::setw(13) << std::setfill('-') << " " << std::setw(11) << std::setfill('-') << " " << std::setw(9) << std::setfill('-') << "\n";
		
		for (int i = 0; i < 6; i++)
		{
			in >> number;
			std::string trash;

			if (in.fail())
			{
				in.clear();
				in.ignore(LLONG_MAX, '\n');
				if (!in.eof())
				{
					out << std::setw(12) << std::setfill(' ') << std::oct << number << " " << std::setw(10) << std::dec << number << " " << std::setw(8) << std::uppercase << std::hex << number << "\n";
				}
			}
			else
			{
				out << std::setw(12) << std::setfill(' ') << std::oct << number << " " << std::setw(10) << std::dec << number << " " << std::setw(8) << std::uppercase << std::hex << number << "\n";
			}
		}
	}

	void PrintMaxFloat(std::istream& in, std::ostream& out)
	{
		float number[6];
		float max;

		for (int i = 0; i < 6; i++)
		{
			in >> number[i];

			if (in.fail())
			{
				in.clear();
				in.ignore(LLONG_MAX, '\n');
				continue;
			}

			out << std::setw(5) << std::setfill(' ') << " " << std::setw(15) << std::setfill(' ') << std::showpos << std::internal << std::fixed << std::setprecision(3) << number[i] << "\n";
		}
		
		max = number[0];

		for (int i = 1; i < 6; i++)
		{
			if (number[i] > max)
			{
				max = number[i];
			}
		}
		out << "max: " << std::setw(15) << std::setfill(' ') << std::showpos << std::internal << std::fixed << std::setprecision(3) << max << "\n";
	}
}